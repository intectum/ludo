/*
 * This file is part of ludo. See the LICENSE file for the full license governing this code.
 */

#ifndef LUDO_TESTS_MATH_VEC_H_
#define LUDO_TESTS_MATH_VEC_H_

namespace ludo
{
  void test_math_vec();
}

#endif /* LUDO_TESTS_MATH_VEC_H_ */
