/*
 * This file is part of ludo. See the LICENSE file for the full license governing this code.
 */

#ifndef LUDO_TESTS_MATH_QUAT_H_
#define LUDO_TESTS_MATH_QUAT_H_

namespace ludo
{
  void test_math_quat();
}

#endif /* LUDO_TESTS_MATH_QUAT_H_ */
