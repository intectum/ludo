/*
 * This file is part of ludo. See the LICENSE file for the full license governing this code.
 */

#ifndef LUDO_TESTS_GRAPHS_LINEAR_OCTREE_H_
#define LUDO_TESTS_GRAPHS_LINEAR_OCTREE_H_

namespace ludo
{
  void test_graphs_linear_octree();
}

#endif /* LUDO_TESTS_GRAPHS_LINEAR_OCTREE_H_ */
