/*
 * This file is part of ludo. See the LICENSE file for the full license governing this code.
 */

#ifndef LUDO_TESTS_BUFFERS_H_
#define LUDO_TESTS_BUFFERS_H_

namespace ludo
{
  void test_buffers();
}

#endif /* LUDO_TESTS_BUFFERS_H_ */
