/*
 * This file is part of ludo. See the LICENSE file for the full license governing this code.
 */

#include "files.h"

namespace ludo
{
  std::string asset_dir = "./assets";
  std::string user_dir = "~/.ludo";
}
