/*
 * This file is part of ludo. See the LICENSE file for the full license governing this code.
 */

#ifndef LUDO_API_H
#define LUDO_API_H

#include "algorithm.h"
#include "animation.h"
#include "buffers.h"
#include "core.h"
#include "data.h"
#include "files.h"
#include "meshes.h"
#include "meshes/edit.h"
#include "meshes/shapes.h"
#include "graphs.h"
#include "input.h"
#include "logging.h"
#include "math/distance.h"
#include "math/mat.h"
#include "math/quat.h"
#include "math/transform.h"
#include "math/util.h"
#include "math/vec.h"
#include "physics.h"
#include "rendering.h"
#include "scripts.h"
#include "tasks.h"
#include "timer.h"
#include "thread_pool.h"
#include "windowing.h"

#endif // LUDO_API_H
